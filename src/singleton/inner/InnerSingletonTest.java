package singleton.inner;


import java.util.Date;

public class InnerSingletonTest {
    public static void main(String[] args) {
        long start = new Date().getTime();
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                InnerSingleton innerSingleton = InnerSingleton.getInstance();
                System.out.println(innerSingleton);
            }).start();
        }
        long end = new Date().getTime();
        System.out.println(end - start);
    }
}
