package singleton.inner;

/**
 * 懒汉式
 * 利用了JVM类加载机制
 * 内部类会在主类加载完成之前实例化
 */
public class InnerSingleton {
    private static class staticClassLazy {
        private static InnerSingleton single = new InnerSingleton();
    }

    private InnerSingleton() {
    }

    public static InnerSingleton getInstance() {
        return staticClassLazy.single;
    }
}
/**
 * 破坏单例的方式：1.克隆；2.反射；3序列化
 **/