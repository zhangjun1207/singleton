package singleton.lazy;


import java.util.Date;

public class LazySingletonTest {
    public static void main(String[] args) {
        long start = new Date().getTime();
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                LazySingleton lazySingleton = LazySingleton.getInstance();
                System.out.println(lazySingleton);
            }).start();
        }
        long end = new Date().getTime();
        System.out.println(end - start);
    }
}
