package singleton.lazy;

/**
 * 懒汉式：若使用频率不高，建议使用，线程不安全，单例实例只有在第一次调用gengInstance方法是初始化；
 * 第一次取实例较慢
 * 双重锁的执行效率更高，而且并发效果更好
 */
public class LazySingleton {

    private LazySingleton() {

    }

    private static volatile LazySingleton lazySingleton = null;

    public static LazySingleton getInstance() {
        if (lazySingleton == null) {
            synchronized (LazySingleton.class) {
                if (lazySingleton == null) {
                    lazySingleton = new LazySingleton();
                }
            }
        }
        return lazySingleton;
    }
}
