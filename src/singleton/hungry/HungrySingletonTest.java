package singleton.hungry;


import java.util.Date;

public class HungrySingletonTest {
    public static void main(String[] args) {
        long start = new Date().getTime();
        for (int i = 0; i<100; i++) {
            HungrySingleton hungrySingleton = HungrySingleton.getInstance();
            System.out.println(hungrySingleton);
        }
        long end = new Date().getTime();
        System.out.println(end-start);
    }
}
