package singleton.hungry;

/**
 * 线程安全，但是占内存，若使用频次不高，不建议使用
 */
public class HungrySingleton {

    private HungrySingleton() {}

    private static final HungrySingleton HUNGRY_SINGLETON = new HungrySingleton();

    public static HungrySingleton getInstance() {
        return HUNGRY_SINGLETON;
    }
}
